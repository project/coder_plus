 SN Coder Plus Description
====================================
SN Coder Plus Drupal module.  Please see
the following page for more current information:

https://drupal.org/sandbox/pgautam/2054327

 Installation
====================================
Regular Drupal module installation.  You
can then choose a configure settings here:

admin/config/system/configure-threshold
admin/config/system/check-complexity

  Credits
====================================
pgautam (coder_plus): https://drupal.org/user/884876
